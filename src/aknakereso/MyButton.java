package aknakereso;

import javafx.scene.control.Button;

public class MyButton extends Button {

    int _i, _j;

    public MyButton(int i, int j) {
        _i = i;
        _j = j;
    }

    public int getI() {
        return _i;
    }

    public int getJ() {
        return _j;
    }
}
