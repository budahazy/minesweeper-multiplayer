package aknakereso;

import java.util.Random;

public class Model {

    Observer observer;

    int[][] mineField = new int[16][16];
    boolean[][] isRevealed = new boolean[16][16];
    int[] playerPoints = new int[2];
    int mineCount;
    int actualPlayer;

    public Model(Observer observer) {
        this.observer = observer;
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                isRevealed[i][j] = false;
            }
        }
        for (int i = 0; i < 2; i++) {
            playerPoints[i] = 0;
        }
        generateMines(51);
        generateNumbers();
        actualPlayer = 0;
        observer.gameStateChanged(mineCount, actualPlayer, playerPoints[0], playerPoints[1]);
    }

    public void peek(final int x, final int y) {
        if (isRevealed[x][y] == false) {
            switch (valueAt(x, y)) {
                case -1:
                    mineCount--;
                    isRevealed[x][y] = true;
                    observer.revealField(x, y, -1, actualPlayer);
                    playerPoints[actualPlayer]++;
                    observer.gameStateChanged(mineCount, actualPlayer, playerPoints[0], playerPoints[1]);
                    break;
                case 0:
                    startRevealAt(x, y);
                    passTurn(actualPlayer);
                    observer.gameStateChanged(mineCount, actualPlayer, playerPoints[0], playerPoints[1]);
                    break;
                default:
                    isRevealed[x][y] = true;
                    observer.revealField(x, y, valueAt(x, y), 0);
                    passTurn(actualPlayer);
                    observer.gameStateChanged(mineCount, actualPlayer, playerPoints[0], playerPoints[1]);
                    break;
            }
        }
    }

    public void revealAllField() {
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                if (!isRevealed[i][j]) {
                    observer.revealField(i, j, valueAt(i, j), -1);
                    isRevealed[i][j] = true;
                }
            }
        }
    }

    private void passTurn(int player) {
        if (player == 0) {
            actualPlayer = 1;
        } else {
            actualPlayer = 0;
        }
        // actualPlayer = (actualPlayer + 1) % 2;
    }

    private int valueAt(final int x, final int y) {
        if (x < 0 || x >= 16 || y < 0 || y >= 16) {
            return -2;
        } else {
            return mineField[x][y];
        }
    }

    private void startRevealAt(final int x, final int y) {
        if (valueAt(x, y) == -2) {
            return;
        }
        if (isRevealed[x][y]) {
            return;
        }

        isRevealed[x][y] = true;
        observer.revealField(x, y, valueAt(x, y), actualPlayer);

        if (mineField[x][y] == 0) {
            for (int i = x - 1; i < x + 2; i++) {
                for (int j = y - 1; j < y + 2; j++) {
                    if (!(x == i && y == j)) {
                        startRevealAt(i, j);
                    }

                }
            }
        }
    }

    private int mineCountAround(final int x, final int y) {
        int mineCount = 0;
        for (int i = x - 1; i < x + 2; i++) {
            for (int j = y - 1; j < y + 2; j++) {
                if (valueAt(i, j) == -1) {
                    mineCount++;
                }
            }
        }
        return mineCount;
    }

    private void generateMines(final int m) {
        mineCount = m;

        Random r = new Random();
        int szamlalo = 0;
        while (szamlalo != m) {
            int i = r.nextInt(16);
            int j = r.nextInt(16);

            if (mineField[i][j] == -1) {
                continue;
            }

            mineField[i][j] = -1;
            szamlalo++;
        }
    }

    private void generateNumbers() {
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                if (mineField[i][j] != -1) {
                    mineField[i][j] = mineCountAround(i, j);
                }
            }
        }
    }
}
