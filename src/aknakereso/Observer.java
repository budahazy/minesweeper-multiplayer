package aknakereso;

public interface Observer {

    void revealField(int x, int y, int v, int p);

    void gameStateChanged(int mineCount, int actualPlayer, int redPoints, int bluePoints);
}
