package aknakereso;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class View extends Application implements Observer {

    Model model;
    Button[][] btns = new Button[16][16];
    Label label = new Label();
    View thisView = this;

    Background loadImage(final String path) {
        return new Background(new BackgroundImage(new Image(path), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT));
    }

    final Background[] images = new Background[]{loadImage("redmine.png"), loadImage("bluemine.png"), loadImage("greenmine.png")};

    @Override
    public void start(Stage primaryStage) {
        Button btnRestart = new Button();
        btnRestart.setText("Új játék!");
        btnRestart.setOnAction((ActionEvent event) -> {
            System.out.println("Új játék elkezdve!");
            resetGameBoard();
            model = new Model(thisView);
        });

        Button btnSolveIt = new Button();
        btnSolveIt.setText("Reveal all field!");
        btnSolveIt.setOnAction((ActionEvent event) -> {
            if (model != null) {
                model.revealAllField();
            } else {
                JOptionPane.showMessageDialog(null, "Először kezdj egy új játékot!", "Hiba!", JOptionPane.ERROR_MESSAGE);
            }
        });

        label.setText("Kezdj egy új játékot!");

        GridPane grid = new GridPane();
        StackPane root = new StackPane();
        VBox vbox = new VBox();

        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                MyButton b = new MyButton(i, j);
                b.setOnAction((ActionEvent event) -> {
                    if (model != null) {
                        model.peek(b.getI(), b.getJ());

                    } else {
                        JOptionPane.showMessageDialog(null, "Először kezdj egy új játékot!", "Hiba!", JOptionPane.ERROR_MESSAGE);
                    }
                });
                btns[i][j] = b;
                b.setMinSize(30, 30);
                b.setStyle("-fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
                grid.add(b, i, j);
            }
        }

        Scene scene = new Scene(root, 480, 600);
        vbox.getChildren().add(label);
        vbox.getChildren().add(btnRestart);
        vbox.getChildren().add(btnSolveIt);
        vbox.getChildren().add(grid);
        root.getChildren().add(vbox);
        primaryStage.setTitle("Zaknakereső!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void revealField(int x, int y, int v, int p) {
        //TODO Create separate css file for button styles
        if (v == -1) {
            if (p == -1) {
                btns[x][y].setStyle(null);
                btns[x][y].setBackground(images[2]);
            } else {
                btns[x][y].setStyle(null);
                btns[x][y].setBackground(images[p]);
            }
        } else {
            btns[x][y].setText(String.valueOf(v));
            switch (v) {
                case 0:
                    btns[x][y].setStyle("-fx-background-color: lightgrey; -fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
                    btns[x][y].setText(null);
                    break;
                case 1:
                    btns[x][y].setStyle("-fx-text-fill: blue; -fx-background-color: lightgrey; -fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
                    break;
                case 2:
                    btns[x][y].setStyle("-fx-text-fill: darkblue; -fx-background-color: lightgrey; -fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
                    break;
                case 3:
                    btns[x][y].setStyle("-fx-text-fill: red; -fx-background-color: lightgrey; -fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
                    break;
                case 4:
                    btns[x][y].setStyle("-fx-text-fill: darkred; -fx-background-color: lightgrey; -fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
                    break;
                case 5:
                    btns[x][y].setStyle("-fx-text-fill: #00994C; -fx-background-color: lightgrey; -fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
                    break;
                case 6:
                    btns[x][y].setStyle("-fx-text-fill: green; -fx-background-color: lightgrey; -fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
                    break;
                case 7:
                    btns[x][y].setStyle("-fx-text-fill: darkgreen; -fx-background-color: lightgrey; -fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
                    break;
                case 8:
                    btns[x][y].setStyle("-fx-text-fill: darkred; -fx-background-color: lightgrey; -fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
                    break;
                default:
            }
        }
    }

    @Override
    public void gameStateChanged(int mineCount, int actualPlayer, int redPoints, int bluePoints) {
        String playerColor;
        if (actualPlayer == 0) {
            playerColor = "PIROS";
        } else {
            playerColor = "KÉK";
        }
        label.setText("Aknák száma:" + String.valueOf(mineCount) + "\nJátékos:" + playerColor + "\nPiros pont:" + redPoints + "\nKék pont:" + bluePoints);
        if (mineCount == 0) {
        }
        if (mineCount == 0) {
            model.revealAllField();
            if (bluePoints < redPoints) {
                JOptionPane.showMessageDialog(null, "Piros nyert!");
            } else {
                JOptionPane.showMessageDialog(null, "Kék nyert!");
            }
        }
    }

    private void resetGameBoard() {
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                btns[i][j].setText(null);
                btns[i][j].setStyle("-fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
            }
        }
    }

}
